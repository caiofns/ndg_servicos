package br.com.catapi.services;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dolcegusto.business.DolceGustoBusiness;


@RestController
@RequestMapping("/servicos")
@EnableAutoConfiguration
public class ControllerCatApi {


	@GetMapping("listarracas/")
	public String consultaManifestacao(@RequestHeader(value = "Accept") String acceptHeader,@RequestHeader(value = "Authorization") String authorizationHeader){
		return "teste";

	}
	
	@GetMapping("getvoucherbyid/{idPromocao}")
	public String getVoucherById(@RequestHeader(value = "Accept") String acceptHeader,@RequestHeader(value = "Authorization") String authorizationHeader, @PathVariable("idPromocao")  String idPromocao){
		
		DolceGustoBusiness dolceGustoBusiness = new DolceGustoBusiness();
		
		String retorno = dolceGustoBusiness.getVoucherById(idPromocao);
		
		return retorno;

	}

}