package br.com.dolcegusto.bean;

public class AnaliseDivergenciaBean {
	
	String dtPedido;
	String pedido;
	String dtChamado;
	String chamado;
	String manifestacao;
	String dtPrimeiroContato;
	String possuiContato;
	String ocorrencia;
	String itensFaltantes;
	String assistente;
	String motivo;
	String cpfCliente;
	String comentarios;
	String statusCd;
	
	public String getDtPedido() {
		return dtPedido;
	}
	public void setDtPedido(String dtPedido) {
		this.dtPedido = dtPedido;
	}
	public String getPedido() {
		return pedido;
	}
	public void setPedido(String pedido) {
		this.pedido = pedido;
	}
	public String getDtChamado() {
		return dtChamado;
	}
	public void setDtChamado(String dtChamado) {
		this.dtChamado = dtChamado;
	}
	public String getChamado() {
		return chamado;
	}
	public void setChamado(String chamado) {
		this.chamado = chamado;
	}
	public String getManifestacao() {
		return manifestacao;
	}
	public void setManifestacao(String manifestacao) {
		this.manifestacao = manifestacao;
	}
	public String getDtPrimeiroContato() {
		return dtPrimeiroContato;
	}
	public void setDtPrimeiroContato(String dtPrimeiroContato) {
		this.dtPrimeiroContato = dtPrimeiroContato;
	}
	public String getPossuiContato() {
		return possuiContato;
	}
	public void setPossuiContato(String possuiContato) {
		this.possuiContato = possuiContato;
	}
	public String getOcorrencia() {
		return ocorrencia;
	}
	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}
	public String getItensFaltantes() {
		return itensFaltantes;
	}
	public void setItensFaltantes(String itensFaltantes) {
		this.itensFaltantes = itensFaltantes;
	}
	public String getAssistente() {
		return assistente;
	}
	public void setAssistente(String assistente) {
		this.assistente = assistente;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getCpfCliente() {
		return cpfCliente;
	}
	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	public String getStatusCd() {
		return statusCd;
	}
	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

}
