package br.com.dolcegusto.bean;

public class VoucherBean {

	String pedido;
	String chamado;
	String manifestacao;
	String dtPrimeiroContato;
	String assistente;
	String motivo;
	String cpfCliente;
	String statusBraspag;
	String comentariosBU;
	String statusVoucher;
	String voucherCode;
	String dtChamado;

	public String getPedido() {
		return pedido;
	}
	public void setPedido(String pedido) {
		this.pedido = pedido;
	}
	public String getChamado() {
		return chamado;
	}
	public void setChamado(String chamado) {
		this.chamado = chamado;
	}
	public String getManifestacao() {
		return manifestacao;
	}
	public void setManifestacao(String manifestacao) {
		this.manifestacao = manifestacao;
	}
	public String getDtPrimeiroContato() {
		return dtPrimeiroContato;
	}
	public void setDtPrimeiroContato(String dtPrimeiroContato) {
		this.dtPrimeiroContato = dtPrimeiroContato;
	}
	public String getAssistente() {
		return assistente;
	}
	public void setAssistente(String assistente) {
		this.assistente = assistente;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getCpfCliente() {
		return cpfCliente;
	}
	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}
	public String getStatusBraspag() {
		return statusBraspag;
	}
	public void setStatusBraspag(String statusBraspag) {
		this.statusBraspag = statusBraspag;
	}
	public String getComentariosBU() {
		return comentariosBU;
	}
	public void setComentariosBU(String comentariosBU) {
		this.comentariosBU = comentariosBU;
	}
	public String getStatusVoucher() {
		return statusVoucher;
	}
	public void setStatusVoucher(String statusVoucher) {
		this.statusVoucher = statusVoucher;
	}
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	public String getDtChamado() {
		return dtChamado;
	}
	public void setDtChamado(String dtChamado) {
		this.dtChamado = dtChamado;
	}

}
