package br.com.dolcegusto.business;

import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.dolcegusto.bean.AnaliseDivergenciaBean;
import br.com.dolcegusto.bean.VoucherBean;
import br.com.dolcegusto.dao.DAO;

public class DolceGustoBusiness {
	
	private DAO dao = new DAO();

	/**
	 * Método responsável por obter lista de vouchers de acordo com a promoção passada via parametro
	 * @author Caio Fernandes
	 * @param String
	 * @return String
	 * */
	public String getVoucherById (String idPromocao) {
		
		ArrayList<VoucherBean> voucherBeanList = new ArrayList<VoucherBean>();
		String jsonRetorno = "";
		
		// 1. REALIZAR BUSCA NA TABELA DE VOUCHERS
		//dao.getVoucherById(idPromocao);
		
		// 2. ARMAZENAR RETORNO NO BEAN DE VOUCHER
		voucherBeanList = populaVoucherBean();
		
		// 3. TRANSFORMAR BEAN DE VOUCHER EM JSON
		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(voucherBeanList);
		} catch (Exception e) {
			System.out.println("[getVoucherById] - ERRO AO CONVERTER BEAN EM JSON");
		}
		
		return jsonRetorno;

	}

	
	/**
	 * Método responsável por popular lista de teste getVoucherById
	 * @author Caio Fernandes
	 * */
	public ArrayList<VoucherBean> populaVoucherBean () {
		
		ArrayList<VoucherBean> voucherBeanList = new ArrayList<VoucherBean>();
		
			
		for (int i = 0; i < 2; i++) {
			
			VoucherBean voucherBean = new VoucherBean();
			
			voucherBean.setPedido("ABR123_" + i);
			voucherBean.setChamado("123123_" + i);
			voucherBean.setManifestacao("12345_" + i);
			voucherBean.setDtPrimeiroContato("02/07/2020" + i);
			voucherBean.setAssistente("assistente_" + i);
			voucherBean.setMotivo("motivo_");
			voucherBean.setCpfCliente("44833855521_" + i);
			voucherBean.setStatusBraspag("statusBraspag_" + i);
			voucherBean.setComentariosBU("comentariosBU_" + i);
			voucherBean.setStatusVoucher("statuVoucher_" + i);
			voucherBean.setVoucherCode("voucherCode_" + i);
			voucherBean.setDtChamado("03/07/2020");
			
			voucherBeanList.add(voucherBean);
		}
		
		return voucherBeanList;
		
	}
	
	public String getAnaliseDivergenciaById(String id) {
		
		ArrayList<AnaliseDivergenciaBean> analiseDivergenciaBeanList = new ArrayList<AnaliseDivergenciaBean>();
		String jsonRetorno = "";
		
		// 1. REALIZAR BUSCA NA TABELA DE ANALISE D.
		
		
		// 2. ARMAZENAR RETORNO NO BEAN DE ANALISE D.
		analiseDivergenciaBeanList = populaAnaliseDivergenciaBean();
		
		// 3. TRANSFORMAR BEAN DE VOUCHER EM JSON
		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(analiseDivergenciaBeanList);
		} catch (Exception e) {
			System.out.println("[getVoucherById] - ERRO AO CONVERTER BEAN EM JSON");
		}
		
		return jsonRetorno;
	}
	
	/**
	 * Método responsável por popular lista de teste getAnaliseDivergenciaById
	 * @author Caio Fernandes
	 * */
	public ArrayList<AnaliseDivergenciaBean> populaAnaliseDivergenciaBean() {
		
		ArrayList<AnaliseDivergenciaBean> analiseDivergenciaBeanList = new ArrayList<AnaliseDivergenciaBean>();
		
			
		for (int i = 0; i < 2; i++) {
			
			AnaliseDivergenciaBean analiseDivergenciaBean = new AnaliseDivergenciaBean();
			analiseDivergenciaBean.setDtPedido("03/07/2020_" + i);
			analiseDivergenciaBean.setPedido("ABR123_");
			analiseDivergenciaBean.setDtChamado("04/07/2020_" + i);
			analiseDivergenciaBean.setChamado("12345_" + i);
			analiseDivergenciaBean.setManifestacao("54321_");
			analiseDivergenciaBean.setDtPrimeiroContato("05/07/2020_" + i);
			analiseDivergenciaBean.setPossuiContato("S_" + i);
			analiseDivergenciaBean.setOcorrencia("ocorrencia_" + i);
			analiseDivergenciaBean.setItensFaltantes("itensFaltantes_" + i);
			analiseDivergenciaBean.setAssistente("assistente_" + i);
			analiseDivergenciaBean.setMotivo("motivo_" + i);
			analiseDivergenciaBean.setCpfCliente("44833855521_" + i);
			analiseDivergenciaBean.setComentarios("comentarios_" + i);
			analiseDivergenciaBean.setStatusCd("statusCd_" + i);
			
			analiseDivergenciaBeanList.add(analiseDivergenciaBean);
		}
		
		return analiseDivergenciaBeanList;
		
	}

}
