package br.com.dolcegusto.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAO {

	/**
	 * Método responsável por realizar a conexão com o banco de dados.
	 * @author Caio Fernandes
	 * @return con
	 * 
	 * */
	public Connection getConexao() {

		Connection con = null;
		String usuario = "ACESSO_NDG";
		String senha = "AC_ACESSO_NDG";
		String nomeBancoDados = "ACESSO_NDG";

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
			con = DriverManager.getConnection("jdbc:sqlserver://SQLPROD:1433;DatabaseName="+nomeBancoDados,	usuario, senha);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return con;
	}

	/**
	 * Método responsável por realizar a consulta de voucher através do código de promoção.
	 * @author Caio Fernandes
	 * 
	 * */
	public ResultSet getVoucherById(String idPromocao) {
		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		ResultSet ret = null;
		try {
			PreparedStatement pst;

			pst = conn.prepareStatement("SELECT * FROM TB_VOUCHERS WHERE ID_PROMOCAO = ?");

			pst.setString(1, idPromocao);

			ret =	pst.executeQuery();

			return ret;



		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ret;
	}

}
