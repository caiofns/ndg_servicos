package br.com.dolcegusto.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dolcegusto.business.DolceGustoBusiness;


@RestController
@RequestMapping("/portalndg")
@EnableAutoConfiguration
public class ControllerDolceGusto {
	
	DolceGustoBusiness dolceGustoBusiness = new DolceGustoBusiness();

	@GetMapping("getvoucherbyid/{idPromocao}")
	public String getVoucherById(@RequestHeader(value = "Accept") String acceptHeader,@RequestHeader(value = "Authorization") String authorizationHeader, @PathVariable("idPromocao")  String idPromocao){
		
		String retorno = dolceGustoBusiness.getVoucherById(idPromocao);
		
		return retorno;

	}
	
	@GetMapping("getanalisedbyid/{id}")
	public String getAnaliseDById(@PathVariable("id")  String id){
		
		String retorno = dolceGustoBusiness.getAnaliseDivergenciaById(id);
		
		return retorno;

	}

}
